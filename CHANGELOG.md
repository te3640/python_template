# Python project template - Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][1], and this project adheres
to [Calendar Versioning](https://calver.org/) with the format YY.MINOR.MICRO.

## [23.1.0]: 

### Added

Project Start

### Removed

### Changed

[1]: https://keepachangelog.com/en/1.0.0/