Main:

[![pipeline status](https://gitlab.com/te3640/python_template/badges/main/pipeline.svg)](https://gitlab.com/te3640/python_template/-/commits/main)
[![coverage report](https://gitlab.com/te3640/python_template/badges/main/coverage.svg)](https://gitlab.com/te3640/python_template/-/commits/main)

# Python Template

Python project template with CI configured

## Requirements

Python3.11


## Create environment

Install dependencies (all):

````shell
make install-all
````

The command will install all dependencies, including the ones for testing, and create a venv

To install only the project dependencies and create a venv, execute:

````shell
make install
````

## Update dependencies

The project comes pre-bundled with a requirements.txt with versions updated at 15-01-2023. It is recommended to update the dependencies, to have the most up to date ones.

We use [pip-tools][1] and requirements.in to manage and pin direct and transitive dependencies.
If you want to add a new dependency, change a version or update them, do it in [requirements.in](requirements/requirements.in) for the app dependencies and in [requirements-test.in](requirements/requirements-tests.in) for test dependencies.
Then, run:

````shell
make update-dependencies
````

And then, to install them in your local environment

````shell
make install-all
````

## Testing and static analysis

For testing purposes [pytest][5] is used.
For coverage, coverage.
For static analysis, [bandit][2] is used for code analysis, [safety][3] for package analysis and [pylint][4] for linter analysis

### To run all tests

````shell
make test
````

### To run a specific test

````shell
make named-test test_name={test_name}
````

### Coverage

````shell
make coverage
````

### Linter

````shell
make linter-static-analysis
````

### Code static analysis

````shell
make code-static-analysis
````

### Package analysis

````shell
make dependencies-static-analysis
````

### Change log

See [CHANGELOG](CHANGELOG.md).


[1]: https://pypi.org/project/pip-tools/
[2]: https://bandit.readthedocs.io/en/latest/#
[3]: https://github.com/pyupio/safety
[4]: https://github.com/PyCQA/pylint
[5]: https://docs.pytest.org/en/latest/