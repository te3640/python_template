create-venv:
	test -d venv || python3.11 -m venv venv

install: create-venv
	. venv/bin/activate && \
	pip install -r requirements.txt --upgrade

install-dev: create-venv
	. venv/bin/activate && \
	pip install -r requirements/requirements-tests.txt --upgrade

install-all: install install-dev

update-dependencies: create-venv
	. venv/bin/activate && \
	pip install --upgrade pip-tools pip setuptools && \
	pip-compile requirements/requirements.in --upgrade --build-isolation --strip-extras -o requirements/requirements.txt --no-emit-options --resolver=backtracking && \
	cp requirements/requirements.txt requirements.txt && \
	pip-compile requirements/requirements-tests.in --upgrade --build-isolation -o requirements/requirements-tests.txt --no-emit-options --resolver=backtracking

test: create-venv
	. venv/bin/activate && \
	python -m pytest tests -vv --capture=tee-sys

named-test: create-venv
ifndef test_name
	$(error test_name value missing. Use: make named-test test_name={test_name})
else
	. venv/bin/activate && \
	python -m pytest tests -vv --capture=tee-sys -k "$(test_name)"
endif

coverage: create-venv
	. venv/bin/activate && \
	coverage run --source . --omit=**/tests/**,setup.py,.venv/*,**/acceptance_tests/**,lib/* -m pytest tests && \
	coverage report --omit=**/tests/**,setup.py,.venv/*,**/acceptance_tests/**,lib/* && \
	coverage xml -o coverage-reports/coverage.xml --omit=**/tests/**,setup.py,.venv/*,**/acceptance_tests/**,lib/* && \
	coverage html -d coverage-reports --omit=**/tests/**,setup.py,.venv/*,**/acceptance_tests/**,lib/*

linter-static-analysis:
	. venv/bin/activate && \
	pylint --disable=C0111 --exit-zero src/*

dependencies-static-analysis:
	. venv/bin/activate && \
	safety check --full-report

code-static-analysis:
	. venv/bin/activate && \
	bandit src/*